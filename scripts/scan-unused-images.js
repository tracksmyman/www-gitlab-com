/* eslint-disable no-prototype-builtins */
// Refer to https://gitlab.com/tywilliams/delete-stale-images for documentation,
// including tests against a minimum reproducible repository.
const fs = require('fs');
const path = require('path');

const isAnImage = (filePath) => {
    const extension = path.extname(filePath);
    return extension === '.png' || extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.svg';
}

const UnusedImageCleaner = {
    // Recursively check the a directory for image files and return their name.
    getSourceImages: function (directory, trimPrefix = false) {
        const sourceImages = [];
        const files = fs.readdirSync(directory);
        files.forEach(file => {
            const filePath = path.join(directory, file);
            const stats = fs.statSync(filePath);
            if (stats.isDirectory()) {
                sourceImages.push(...UnusedImageCleaner.getSourceImages(filePath));
            } else if (isAnImage(filePath)) {
                sourceImages.push(filePath);
            }
        });

        if (trimPrefix) {
            return sourceImages.map(image => image.replace('source/', ''));
        }

        return sourceImages;
    },

    // Check a file for image references and return their names in an array
    getImageReferencesFromFileContents: function (filePath) {
        try {
            const fileContents = fs.readFileSync(filePath, 'utf8');
            return fileContents.match(/images\/.*\.(jpg|jpeg|png|gif|svg)/g);
        } catch (error) {
            console.error(`Error reading file ${filePath}: ${error}`);
            return [];
        }

    },

    // Recursively read every file in the directory, and return all references to images.
    getImageReferences: function (directory, ignoreList = []) {
        console.log(`Checking ${directory}`);
        const imageReferences = [];
        const files = fs.readdirSync(directory);
        files.forEach((file, index) => {
            const filePath = path.join(directory, file);
            // Skip if the filePath is in the ignorelist
            if (ignoreList.includes(filePath)) {
                return;
            }

            const stats = fs.statSync(filePath);
            if (stats.isDirectory()) {
                imageReferences.push(...UnusedImageCleaner.getImageReferences(filePath));
            } else {
                const references = UnusedImageCleaner.getImageReferencesFromFileContents(filePath);
                // Add the contents of the references array to imageReferences
                if (references) {
                    imageReferences.push(...references);
                }
            }
        });

        return imageReferences;
    },

    getUnusedImages: function (sourceImages, imageReferences) {
        // Create an object from sourceImagesObject, where each key is the image path and the value is 0
        const sourceImagesObject = {};
        sourceImages.forEach(function (image) {
            sourceImagesObject[image] = 0;
        });

        // Loop through imageReferences, and increment the value of the image path in sourceImagesObject
        imageReferences.forEach(function (imageReference) {
            const imageWithLeadingSlash = '/' + imageReference;
            if (sourceImagesObject.hasOwnProperty(imageReference) || sourceImagesObject.hasOwnProperty(imageWithLeadingSlash)) {
                sourceImagesObject[imageReference]++;
            }
        });

        // Create an array of image paths that have a value of 0
        const unusedImages = [];
        for (let image in sourceImagesObject) {
            if (sourceImagesObject[image] === 0) {
                unusedImages.push(image);
            }
        }

        return unusedImages;
    }
}

// Get the file path that's the parent folder of this one
const parentDirectory = path.resolve(__dirname, '..');

// Check if a /tmp folder exists, and if not, create it
const tmpDirectory = path.join(parentDirectory, 'tmp');
if (!fs.existsSync(tmpDirectory)) {
    fs.mkdirSync(tmpDirectory);
}

// Delete tmp files if they exist, since we don't want double count references.
const outputFiles = [
    path.join(parentDirectory, 'tmp', 'source-images.txt'),
    path.join(parentDirectory, 'tmp', 'image-references.txt'),
    path.join(parentDirectory, 'tmp', 'unused-images.txt'),
]

outputFiles.forEach(file => {
    if (fs.existsSync(file)) {
        fs.unlinkSync(file);
    }
});

// File path should be: '../source/images'
const sourceImages = UnusedImageCleaner.getSourceImages('source/images', true);
const imageReferences = UnusedImageCleaner.getImageReferences(parentDirectory);
const unusedImages = UnusedImageCleaner.getUnusedImages(sourceImages, imageReferences);
const unusedImagesWithPrefix = unusedImages.map(image => 'source/' + image);

// Write list of source images to a file
fs.writeFileSync('./tmp/source-images.txt', sourceImages.join('\n'));

// Write list of imageReference to a file
fs.writeFileSync('./tmp/image-references.txt', imageReferences.join('\n'));

// Write unusedImages to ../tmp/unused-images.txt, create it if it doesn't exist
fs.writeFileSync('./tmp/unused-images.txt', unusedImagesWithPrefix.join('\n'));

console.log('Check ./tmp/unused-images.txt to see the results of the scan.');
