---
layout: handbook-page-toc
title: "TAM Segment: Named"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

⚠️ This information is under active development and iteration. This page will be updated on an ongoing basis as the named team continues to grow and scale.
{: .alert .alert-warning}

## Overview

Definition: Named TAM on the account, product usage data-based success goals per account, programmatic enablement.

## Engagement Model

![Named Customer Lifecycle Management](/images/handbook/customer-success/Customer_Lifecycle_Journey_Named_TAM.png)



## Metrics

### Align

Success Plans are more focused on quantitative objectives such as use case adoption, tied into value drivers as determined in the pre-sales process, to ensure platform value is realized. Progress and success against these objectives are reported upon in the EBRs that will be held with those customers that have product usage data available.

#### Metrics for Align

1. 100% net-new customers with success plans (value drivers, use cases)
1. EBRs held with 50% of customers with product usage data
1. EBRs held with 50% of Priority 1 customers

### Enable

Onboarding and enablement for this cohort is primarily through webinar cohorts, with TAM touchpoints throughout the first 60 days to ensure the customer is set up for success and has overcome initial roadblocks to adoption and value. TAM provides enablement webinars and adhoc sessions to ensure the customer has key adoption questions answered and has the best practice guidance needed to be successful. Use Case Health Scores enable the TAM to track the efficacy of these programs and determine strategic reach-outs.

#### Metrics for Enable

Onboarding:

1. Time to first engage: within 14 days
1. Time to 1st Value: within 30 days
1. Time to Onboard: within 45 days
1. Attendance in onboarding webinars of at least 50% of net-new customers
1. Average onboarding NPS & CSAT scores over 4.0 (out of 5)
1. 100% of net-new customers have green success plans within 60 days
1. Primary use case health scores green within first 90 days

### Expand & Renew

Expansion is primarily driven by the SAL or AE in this segment, though a key driver for expansion is product enablement and familiarity.  Webinars are the primary means of driving interest for customers into new segments, the success of which is tracked through customer engagement scorecards and product usage data insights around new use cases adopted. Low-license utilization reports will focus the TAM on identifying those customers at risk of contraction. The renewal NPS/CSAT survey 110 days before renewal enables the TAM to identify customers that may be challenged at the point of renewal and are not easily identifiable as challenged through product usage data.

#### Metrics for Expand & Renew

1. Consistent enablement and expansion webinars and attendance
   1. 3 webinars per quarter
   1. 80% of attendees stay for entire webinar
   1. Average feedback score of webinars over 4.0 (out of 5)
1. Consistent stage adoption efforts and adoption
   1. 50% of accounts have an open and active stage adoption play
   1. At least two stages are adopted for 90% of customers
   1. 10% increase of stages adopted across customer base quarter over quarter
   1. Initial adoption of a stage (10% of users) within 60 days of enablement
   1. Broad adoption of a stage (50% of users) within 6 months of enablement
1. % Low License Utilizations 'Saves' (improvement)
1. Renewal NPS & CSAT Scores over 4.0 (out of 5)
